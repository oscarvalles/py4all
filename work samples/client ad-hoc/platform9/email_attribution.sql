-- create base table
create table ba.platform9_conversion_pixels as
select distinct
nexus_id,
json_extract_scalar(referrer_params,'$.email') as email,
split_part(json_extract_scalar(referrer_params,'$.email'),'@',2) as domain,
request_date
from default.pixelator_data p
where p.tracker = '2g9r'
;

drop table if exists ba.platform9_email_sends;
create table ba.platform9_email_sends as
select s.account_name,
s.opportunity_name,
s.oli,
u.id as user_id,
cs.recipient_id,
r.real_email,
split_part(r.real_email,'@',2) as domain,
cast(cast(cc.sent_at as date) as varchar) event_date
from ba.salesforce_ios s
inner join psql_imports.campaigns c
 on regexp_replace(s.oli,'[^0-9]','') = regexp_replace(c.oli,'[^0-9]','')
inner join psql_imports.cm_campaigns cc
 on c.cm_id = cc.cm_id
inner join psql_imports.cm_sends cs
 on cs.cm_campaign_id = cc.id
inner join psql_imports.mb_recipients r
 on cs.recipient_id = r.id
inner join psql_imports.email_addresses e
 on r.real_email = e.email
inner join psql_imports.users u
 on u.email_address_id = e.id
where s.oli in ('OLI-266401','OLI-266378','OLI-269361','OLI-268577','OLI-269363','OLI-267389','OLI-266389')
;

drop table if exists ba.platform9_touchpoints_stacked;
create table ba.platform9_touchpoints_stacked as
select p.*, 'user_id to nexus match' as attribution_type
from ba.platform9_conversion_pixels p
inner join default.id_mapping_nexus n
 on p.nexus_id = n.nexus
inner join ba.platform9_email_sends s
 on n.user = s.user_id
where n.latest
and n.user is not null
and s.event_date <= p.request_date

union

select p.*, 'pixel referrer email match' as attribution_type
from ba.platform9_conversion_pixels p
inner join ba.platform9_email_sends s
 on p.email = s.real_email
where s.event_date <= p.request_date

union

select p.*, 'pixel referrer email domain match' as attribution_type
from ba.platform9_conversion_pixels p
inner join ba.platform9_email_sends s
 on p.domain = s.domain
where s.event_date <= p.request_date

union

select p.*, 'base' as attribution_type
from ba.platform9_conversion_pixels p
;


-- discuss result export options
select date_trunc('month', cast(request_date as date)) as pixel_month,
count( distinct case when attribution_type = 'pixel referrer email match' then nexus_id || request_date else null end) as spiceworks_email_attrib,
count(distinct nexus_id || request_date) as pixel_fires
from ba.platform9_touchpoints_stacked group by 1 order by 1
