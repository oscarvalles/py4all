﻿-- Contest Summaries 
with geos as
(
select t.id, regexp_split_to_table(t.subject, E'\\s+') as geo
from ba.community_group_summary cgs
inner join topics t
  on cgs.forum_id = t.forum_id
where cgs.category_tag = 'fun'
and cgs.group_tag = 'contests'
and extract('year' from t.created_at) >= extract('year' from current_date - interval '1 year')
and t.deleted = 0
--and regexp_split_to_table(t.subject, E'\\s+') in ('UK','DE','FRANCE')
), international_contests as
(
select id as topic_id, regexp_replace(geo,'[^a-zA-Z]','') as geo
from geos
where lower(regexp_replace(geo,'[^a-zA-Z]','')) in ('uk', 'de', 'fr', 'es', 'italy', 'nl', 'se', 'dk', 'ru', 'po', 'ch','ca',
'united kingdom','germany','france','spain','netherlands','sweden','denmark','russia','poland','china','emea')
group by 1,2
), all_contests as
(
select cgs.category_url, cgs.forum_name, cgs.group_url, t.ranking as spiceups, t.post_counter as replies, t.subject, t.id as topic_id, t.created_at as contest_start_date,
case 
  when substring(lower(t.subject), '(amazon|gift card| gc |giftcard)') is not null then 'gift card'
  when substring(lower(t.subject), '(xbox|ps4|nintendo|game)') is not null then 'game console'
  when substring(lower(t.subject), '(droid|apple|samsung|galaxy|camera|bose|ipad|drone|phone|gopro|go pro|bluetooth|watch|robot|lego)') is not null then 'electronics'
  when substring(lower(t.subject), '(computer|server|laptop|tablet|dell|hp |lenovo|printer|router|switch|hpe|raspberry|chromebook|thinkpad)') is not null then 'hardware'
  when substring(lower(t.subject), '(trip|travel|vacation|conference|airline|ticket to)') is not null then 'trip'
  when substring(lower(t.subject), '(network|app|software|antivirus|download|install|ransomware|windows)') is not null then 'software'
  when t.subject not ilike '%win%' then 'unspecified prize'
  when substring(lower(t.subject), '(prize|umbrella|lego|hammock|earbud|headset|shirt|backpack|gear|swag|movie)') is not null then 'general merchandise'  
  else 'other' 
end as prize_type,
substring(t.subject,'(\$[1-9,][0-9,]{1,7})') as prize_value
from ba.community_group_summary cgs
inner join topics t
  on cgs.forum_id = t.forum_id
where cgs.category_tag = 'fun'
and cgs.group_tag = 'contests'
and extract('year' from t.created_at) >= extract('year' from current_date - interval '1 year')
and t.deleted = 0
)

select ac.topic_id as contest_id
,'https://community.spiceworks.com/topic/' || ac.topic_id as contest_url
,ac.subject as contest
,ac.replies
,ac.spiceups
,round((ac.spiceups * 1.0)/ ac.replies,1) * 100 as popularity_pct
,coalesce(case 
  when ac.subject ilike '%us only%' then 'USA'
  when ac.subject ilike '%us & ca%' then 'USA/CANADA'
  when ac.subject ilike '%us and canada%' then 'USA/CANADA'
  else ic.geo
 end,'Any Geo') as geo
,ac.contest_start_date::date as contest_start_date
,prize_type
,prize_value
from all_contests ac
left join international_contests ic
 on ac.topic_id = ic.topic_id
