"""
# JumpCloud Daily Reporting
# To be scheduled 10:00 am daily
"""

# import modules
import psycopg2 as pg, pandas as pd, numpy as np, re, datetime as dt
from pyhive import presto
from datetime import timedelta

# connections and cursors
biDbConn = pg.connect(host='warehouse', database='bi_db', user='oscarv')
biDbConn.set_isolation_level(pg.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
biDbCursor = biDbConn.cursor()
ahConn = pg.connect(host='warehouse', database='analytics_historical', user='oscarv')
ahCursor = ahConn.cursor()

from sqlalchemy import create_engine
biDbEngine = create_engine('postgresql://oscarv@warehouse:5432/bi_db')
ahEngine = create_engine('postgresql://oscarv@warehouse:5432/analytics_historical')

"""
# Queries for Pixel and Banner Data Sets
"""

# declare variables and queries
day = dt.datetime.now().date()  # used to retrieve data for reports
report_date = dt.datetime.now().date() - timedelta(days=day.weekday()) + timedelta(days=4) # 2 days ago

ipAddresses = ""
biDbQueries = {
    "Jumpcloud Pixel":"""select
       request_params::json->>'id' as id
       ,nexus_id
       ,ip as ip_address
       ,country_code
       ,request_date
       ,row_number() over (partition by request_params::json->>'id' , request_date
                        order by request_params::json->>'id' , request_date) transaction_row
       from ba_pixels.pixelator_data_ik00
       where request_date between current_date - interval '1 week' and current_date
       and country_code in ('US','CA','MX','Unknown')""",
    "Banners Off Network":"""select distinct nexus_id, ip_address, clicks, impression_date
           from ba_clicks_impressions.beeswax_001c0000018lifniak
           where impression_date between current_date - interval '6 month' and current_date""",
    "Banners On Network":"""select distinct nexus_id, ip_address, clicks, event_date
           from ba_clicks_impressions.on_network_001c0000018lifniak
           where event_date between current_date - interval '6 month' and current_date"""
}

# compile datasets
pixelFires = pd.read_sql(biDbQueries['Jumpcloud Pixel'], biDbConn)
pixelFires = pixelFires[pixelFires.transaction_row == 1] # keep the unique transaction
pixelFires['request_date'] = pd.to_datetime(pixelFires['request_date'])
pixelFires.dropna(inplace=True)
del pixelFires['transaction_row']
# Banner Impressions
offNetwork = pd.read_sql(biDbQueries['Banners Off Network'], biDbConn)
offNetwork['impression_date'] = pd.to_datetime(offNetwork['impression_date']) # convert to date type

onNetwork  = pd.read_sql(biDbQueries['Banners On Network'], biDbConn)
onNetwork['event_date'] = pd.to_datetime(onNetwork['event_date']) # convert to date type

"""
# Create Subsets for Later Merge
"""
# subset of Banner Impressions by nexus and ips (Off Network)
offNetworkIps = offNetwork[['ip_address', 'impression_date']].copy()
offNetworkIps.rename(columns = {'impression_date':'offNetIpDate'}, inplace=True)
offNetworkIps['offNetIps'] = 1
offNetworkNexus = offNetwork[['nexus_id', 'impression_date']].copy()
offNetworkNexus.rename(columns = {'impression_date':'offNetNexusDate'}, inplace=True)
offNetworkNexus['offNetNexus'] = 1

# subset of Banner Impressions by nexus and ips (On Network)
onNetworkIps = onNetwork[['ip_address', 'event_date']].copy()
onNetworkIps.rename(columns = {'event_date':'onNetIpDate'}, inplace=True)
onNetworkIps['onNetIps'] = 1
onNetworkNexus = onNetwork[['nexus_id', 'event_date']].copy()
onNetworkNexus.rename(columns = {'event_date':'onNetNexusDate'}, inplace=True)
onNetworkNexus['onNetNexus'] = 1

"""
# Get Community overlap for 6 months
"""
# retrieve list of IP Addresses from Pixel
ipAddresses = pixelFires['ip_address'].unique().tolist()
ipAddresses = "','".join(str(n) for n in ipAddresses)

# Query for SW Visitor that has been here 6 months ago
swVisitorQuery = """select ip_address, last_visited, 1 as community_visit
     from production.user_to_ips
     where last_visited >= cast(current_date - interval '12 month' as varchar)
     and last_visited <= cast(current_date as varchar)
     and ip_address in ('%s')""" % ipAddresses
# Spiceworks Visitor

swVisitor  = pd.read_sql(swVisitorQuery, biDbConn)
swVisitor['last_visited'] = pd.to_datetime(swVisitor['last_visited'])

"""
# Begin Finding Overlap
"""

# overlap on Off Network IP Address
overlap01 = pd.merge(pixelFires, offNetworkIps, on='ip_address', how='inner')
overlap01['days_diff'] = (overlap01['request_date'] - overlap01['offNetIpDate']).dt.days
overlap01 = overlap01[overlap01.days_diff >= 0] # remove attribution that did not preceed
del overlap01['offNetIpDate'], overlap01['days_diff']
overlap01.drop_duplicates(inplace=True) # removes duplicates

# overlap on Off Network Nexus Ids
overlap02 = pd.merge(pixelFires, offNetworkNexus, on='nexus_id', how='inner')
overlap02['days_diff'] = (overlap02['request_date'] - overlap02['offNetNexusDate']).dt.days
overlap02 = overlap02[overlap02.days_diff >= 0] # remove attribution that did not preceed
del overlap02['offNetNexusDate'], overlap02['days_diff']
overlap02.drop_duplicates(inplace=True) # removes duplicates

# Spiceworks Visitor - Visited Community
overlap03 = pd.merge(pixelFires, swVisitor, on='ip_address', how='inner')
overlap03['days_diff'] = (overlap03['request_date'] - overlap03['last_visited']).dt.days
overlap03 = overlap03[overlap03.days_diff >= 0] # remove attribution that did not preceed
del overlap03['last_visited'], overlap03['days_diff']
overlap03.drop_duplicates(inplace=True) # removes duplicates

# overlap on On Network IP Address
overlap05 = pd.merge(pixelFires, onNetworkIps, on='ip_address', how='inner')
overlap05['days_diff'] = (overlap05['request_date'] - overlap05['onNetIpDate']).dt.days
overlap05 = overlap05[overlap05.days_diff >= 0] # remove attribution that did not preceed
del overlap05['onNetIpDate'], overlap05['days_diff']
overlap05.drop_duplicates(inplace=True) # removes duplicates

# overlap on On Network Nexus Ids
overlap06 = pd.merge(pixelFires, onNetworkNexus, on='nexus_id', how='inner')
overlap06['days_diff'] = (overlap06['request_date'] - overlap06['onNetNexusDate']).dt.days
overlap06 = overlap06[overlap06.days_diff >= 0] # remove attribution that did not preceed
del overlap06['onNetNexusDate'], overlap06['days_diff']
overlap06.drop_duplicates(inplace=True) # removes duplicates

# stack the datasets
stackedDfs = pd.concat([pixelFires, overlap01, overlap02, overlap03, overlap05, overlap06])
stackedDfs = stackedDfs[['id','nexus_id','ip_address','country_code','request_date','offNetIps','offNetNexus','onNetIps','onNetNexus','community_visit']]
stackedDfs.fillna(0, inplace=True)
stackedDfs.head()

# Reorder column names
stackedDfs[stackedDfs.offNetIps == 1].head()
stackedDfs = stackedDfs.groupby(['id','nexus_id','country_code','request_date'], as_index=False).agg(
    {'offNetIps': max, 'offNetNexus':max, 'onNetIps':max, 'onNetNexus':max, 'community_visit':max})
stackedDfs = stackedDfs[['id','nexus_id','country_code','request_date','offNetIps','offNetNexus','onNetIps','onNetNexus','community_visit']]
stackedDfs.head()

# create variables to hold conditionally derived variables
stackedDfs['banner_attribution'] = 'No'
stackedDfs['off_network'] = 'No'
stackedDfs['on_network'] = 'No'
stackedDfs['spiceworks_visitor'] = 'No'

# fill conditional variables
stackedDfs.loc[(stackedDfs.offNetIps==1.0) |
               (stackedDfs.offNetNexus==1.0) |
               (stackedDfs.onNetNexus==1.0) |
               (stackedDfs.onNetIps==1.0),"banner_attribution"] = "Yes"
stackedDfs.loc[(stackedDfs.offNetIps==1.0) |
               (stackedDfs.offNetNexus==1.0),"off_network"] = "Yes"
stackedDfs.loc[(stackedDfs.onNetNexus==1.0) |
               (stackedDfs.onNetIps==1.0),"on_network"] = "Yes"
stackedDfs.loc[(stackedDfs.community_visit==1.0) |
               (stackedDfs.onNetNexus==1.0) |
               (stackedDfs.onNetIps==1.0),"spiceworks_visitor"] = "Yes"
stackedDfs.loc[(stackedDfs.community_visit==1.0) |
               (stackedDfs.offNetNexus==1.0) |
               (stackedDfs.offNetIps==1.0),"spiceworks_visitor"] = "Yes"

"""
# Write the days results to Postgres
"""
writeToBiDb = stackedDfs[stackedDfs.request_date == report_date]
writeToBiDb.to_sql('jumpcloud_reporting', biDbEngine, schema='oscarv', index=False, if_exists='append')
