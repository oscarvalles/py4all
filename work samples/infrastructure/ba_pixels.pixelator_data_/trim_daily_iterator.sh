for i in {2..90} # start at 2 and go back as many days as you would like to load of data
  do param_date=$(date -v -${i}d +%Y-%m-%d);
  START_DATE=${param_date} END_DATE=${param_date} TRACKERS='iaja' python trim_pixelator_data.py
  # echo ${param_date};
done
