import  pandas as pd, numpy as np, time, datetime as dt, psycopg2 as pg, os, csv, codecs, sys

class pixelator_data():
    def __init__(self):
        return
    # create a mapping table between SFDC account id and trackers.  Until then, don't call this function
    def trackers(self, account_id, conn):
        dfpLineItemsQuery = """select distinct d.line_item_id
            from ba.salesforce_ios s
            inner join ba.trackers d
            on d.io = s.io
            where s.account_id in ('""" + account_id + """')
            and d.line_item_id is not null"""

        dfpLineItems = pd.read_sql_query(dfpLineItemsQuery, conn)

        # save line items into a string to input later
        dfpLineItemList = dfpLineItems['line_item_id'].tolist()
        # dfpLineItemList = ",".join(map(str, dfpLineItemList))
        dfpLineItemList = ','.join(["(cast('%s' as bigint))" % line_item for line_item in dfpLineItemList])
        return dfpLineItemList

    def pixelatorDataToCsv(self, startPeriod, endPeriod, tracker, conn):
        pixelatorDataQuery = """select nexus_id,
                                      request_time,
                                      referrer,
                                      referrer_params,
                                      request_params,
                                      ip,
                                      country_code,
                                      request_date,
                                      tracker,
                                      current_date as refresh_date
                                      from pixelator_data
                                      where request_date between '%s' and '%s'
                                      and tracker in ('%s')""" % (startPeriod, endPeriod, tracker)
        beeswaxImpressions = pd.read_sql_query(pixelatorDataQuery, conn) # save results to pandas DF
        beeswaxImpressions.to_csv(tracker + '.csv', index=False, header=False)

    def loadPixelatorDataCsv(self, pgConn, dbSchema, tracker):
        pgCursor = pgConn.cursor()
        try:
            f = open(tracker + ".csv")
            print ("file opened: " + str(f))
            dropIndex = """drop index if exists %s.idx_%s_pixelator_data_%s_request_date""" % (dbSchema, dbSchema, tracker)
            pgCursor.execute(dropIndex)
            print dropIndex
            pgConn.commit()
            pgCursor.copy_expert("copy " + dbSchema + ".pixelator_data_" + tracker + " FROM STDIN WITH DELIMITER ',' CSV", f)
            pgConn.commit()
            print("data loaded")
            f.close()
            os.remove(tracker + ".csv")
            print("file removed")
            createIndex = """create index idx_%s_pixelator_data_%s_request_date on %s.pixelator_data_%s (request_date)""" % (dbSchema, tracker,dbSchema, tracker)
            pgCursor.execute(createIndex)
            pgConn.commit()
            print createIndex
        except Exception as e:
            print("File not uploaded: " + str(e))
            f.close()
            pgConn.rollback()


    def loadPixelatorDataOrCreate(self, pgConn, dbSchema, tracker, action):
        pgCursor = pgConn.cursor()
        if action == 'create':
            print 'creating table'
            pgCursor.execute("select distinct advertiser_name || ' - ' || left(pixel_name,25) || '... ' as advertiser_name from ba.pixel_tracker where pixel_code = '%s'" % (tracker))
            gitlab_url = 'https://gitlab.spice.spiceworks.com/business_analytics/infrastructure/tree/master/warehouse/bi_db/ba_pixels.pixelator_data_'
            account_name = pgCursor.fetchall()
            account_name = account_name[0]
            account_name = ''.join(map(str, account_name))
            account_name =  account_name.replace("'","")
            pgQueries = {
            "001 DROP PIXEL_TRACKERS":"drop table if exists %s.pixelator_data_%s" % (dbSchema, tracker),
            "002 CREATE PIXEL_TRACKERS":"""create table %s.pixelator_data_%s
            (
            nexus_id varchar(255),
            request_time bigint,
            referrer text,
            referrer_params text,
            request_params text,
            ip varchar(200),
            country_code varchar(50),
            request_date date,
            tracker varchar(10),
            refresh_date date
            )""" % (dbSchema, tracker),
            "003 OWNERSHIP":"""ALTER TABLE %s.pixelator_data_%s OWNER TO usergroup_ba""" % (dbSchema, tracker),
            "004 USERGROUP_BA":"""GRANT ALL ON TABLE %s.pixelator_data_%s TO usergroup_ba""" % (dbSchema, tracker),
            "005 USERGROUP_MKT_RESEARCH":"""GRANT ALL ON TABLE %s.pixelator_data_%s TO usergroup_market_research""" % (dbSchema, tracker),
            "006 TABLE COMMENT":"""COMMENT ON TABLE %s.pixelator_data_%s
            IS 'Author: Oscar Valles.  %s Pixelator Data from ad-cluster. Source: %s. '""" % (dbSchema, tracker, account_name, gitlab_url),
            "007 INDEX":"""create index idx_%s_pixelator_data_%s_request_date on %s.pixelator_data_%s (request_date)""" % (dbSchema, tracker,dbSchema, tracker)
            }
            # execute above queries to create tables
            for d,q in sorted(pgQueries.iteritems()):
                try:
                    pgCursor.execute(q)
                    pgConn.commit()
                except Exception as e:
                    print("Postgres error: " + str(e))
                    pgConn.rollback()
            # load impressions
            self.loadPixelatorDataCsv(pgConn, dbSchema, tracker)
        else:
            print 'loading file'
            self.loadPixelatorDataCsv(pgConn, dbSchema, tracker)

        message = 'pixelator_data' + tracker + ' loaded'
        return message

    def csvPixelatorDataToTable(self, pgConn, dbSchema, tracker):
        dbCursor = pgConn.cursor()
        dbCursor.execute("""select count(*)
            from information_schema.columns
            where table_schema = '%s'
            and table_name = 'pixelator_data_%s'""" % (dbSchema, tracker.lower()))
        result = dbCursor.fetchone()

        if result[0] > 0:
            msg = 'preparing to load file'
            action = 'load'
            self.loadPixelatorDataOrCreate(pgConn, dbSchema, tracker, action)
            dbCursor.close()
            return msg
        else:
            msg = 'creating table %s.pixelator_data_%s for file load' % (dbSchema, tracker.lower())
            action = 'create'
            self.loadPixelatorDataOrCreate(pgConn, dbSchema, tracker, action)
            dbCursor.close()
            return msg
