# import needed libraries
import time, datetime as dt, psycopg2 as pg, os, csv, codecs, sys
from datetime import timedelta
from pyhive import presto

from pyhive import presto
# custom library

from inc.trim_pixels import pixelator_data
reload(sys)
sys.setdefaultencoding('utf-8')

# Establish Connections and Cursors
clusterConn = presto.connect('ad-cluster1',port=8083)
clusterCursor = clusterConn.cursor()

pgConnBidb = pg.connect(host='warehouse', database='bi_db', user='oscarv')
pgCursorBidb = pgConnBidb.cursor()

pgConnAh = pg.connect(host='warehouse', database='analytics_historical', user='oscarv')
pgCursorAh = pgConnAh.cursor()

# time components for report
day = dt.datetime.now().date()
# default impression dates
if not os.getenv('START_DATE'):
    start_date = dt.datetime.now().date() - timedelta(days=2, weeks=0) # two days ago
    start_date = str(start_date)
else:
    start_date = os.getenv('START_DATE')

if not os.getenv('END_DATE'):
    end_date =   dt.datetime.now().date() - timedelta(days=1, weeks=0) # yesterday
    end_date = str(end_date)
else:
    end_date = os.getenv('END_DATE')

# default schema
if not os.getenv('SCHEMA'):
    schema = 'ba_pixels'
else:
    schema = os.getenv('SCHEMA')

# accounts to bring in
# default schema
if not os.getenv('TRACKERS'):
    # Below: Snow Software, Jumpcloud, Automox, CDW, HPI
    trackers = ['2h5s','iaja','disg','ik00']
else:
    trackers = os.getenv('TRACKERS')
    trackers = trackers.split()

print start_date, end_date, schema

# initialize class
pixelator_data = pixelator_data()

for tracker in trackers:
    pixelator_data.pixelatorDataToCsv(start_date, end_date, tracker, clusterConn)
    print 'csv exported'
    pixelator_data.csvPixelatorDataToTable(pgConnBidb, schema, tracker)
