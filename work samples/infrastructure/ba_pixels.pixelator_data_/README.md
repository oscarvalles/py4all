# ba_pixels.pixelator_data_

# Purpose
Ingest pixel fires by tracker and place into individual tables specific to trackers.  This makes it easier when analyzing for customer and campaign specific reporting and analysis.  To see a list of pixels that are schedules on a daily basis,
run the following query in bi_db.

Breaking apart pixel data into tracker specific tables helps mitigate performance issues when working in bi_db (Postgres)

```
select i.pixel_code, i.load_type, t.advertiser_id, t.advertiser_name, t.pixel_name
from ba_pixels.interim_scheduled_pixels i
inner join ba.pixel_tracker t
 on i.pixel_code = t.pixel_code
order by 1
```
#### adding a pixel to the daily load schedule

### sql
In bi_db run either statement:
```
1.) insert into ba_pixels.interim_scheduled_pixels values ('pixel_code','full') -- to capture all pixel fields
2.) insert into ba_pixels.interim_scheduled_pixels values ('pixel_code','trimmed') -- for high volume pixels
```

Note: Pixels with a load_type of "full", will include all fields from the ad-cluster's pixelator_data.  
      Pixels with a load_type of "trimmed" will exclude certain fields such as referrer params for performance purposes.
      Please test out any pixel you wish to add prior to adding into "ba_pixels.interim_scheduled_pixels" for scheduling

#### "trimmed" pixel loads are under development


# Scheduled

  - Runs daily at 10:pm CST
  - Creates pixel table if it does not already exist
  - Loads daily data if exists

### loading historical data
To load historical data, use bash script "daily_iterator.sh" by editing the following:
```
line 1: {1..20} # represent the starting offset of days back to the furthest number of days minus today's date.  creates a date range for backfill
line 4: TRACKERS='insert pixel code/tracker here' # set the tracker that needs to be iterated through
```

### specific daily load
Navigate to folder containing "daily_iterator.sh"
```
START_DATE='2018-05-01' END_DATE='2018-05-01' TRACKERS='disg' python pixelator_data.py # 2-7 day date ranges can be provided for smaller volume pixels
```

#### cron
```
00 22 * * *
```
