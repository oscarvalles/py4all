for i in {2..30} # start at 2 and go back as many days as you would like to load of data
  # do param_date=$(date -d ${i}' days ago' +%Y-%m-%d); # works on Ubuntu
  do param_date=$(date -v -${i}d +%Y-%m-%d); # works on OS X
  START_DATE=${param_date} END_DATE=${param_date} SFDC_ACCOUNT_ID='001C0000018LiFnIAK' python on_network_impressions.py
  # echo ${param_date};
done
