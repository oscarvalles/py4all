import  pandas as pd, numpy as np, time, datetime as dt, psycopg2 as pg, os, csv, codecs, sys

class onNetwork():
    def __init__(self):
        return

    def dfp_line_items(self, account_id, conn):
        dfpLineItemsQuery = """select distinct d.line_item_id
            from ba.salesforce_ios s
            inner join ba.dfp_line_items d
            on d.io = s.io
            where s.account_id in ('""" + account_id + """')
            and d.line_item_id is not null"""

        dfpLineItems = pd.read_sql_query(dfpLineItemsQuery, conn)

        # save line items into a string to input later
        dfpLineItemList = dfpLineItems['line_item_id'].tolist()
        # dfpLineItemList = ",".join(map(str, dfpLineItemList))
        dfpLineItemList = ','.join(["(cast('%s' as bigint))" % line_item for line_item in dfpLineItemList])
        return dfpLineItemList

    def onNetworkBannersToCsv(self, account_id, startPeriod, endPeriod, dfp_line_items, conn):
        onNetworkBannersQuery = """select distinct i.nexus_id, u.ip_address, i.creative_id, i.line_item,
                                       i.dfp_advertiser_id, i.ad_unit, i.timestamp, 0 as clicks, i.impression_date as event_date
                                       from default.dfp_impressions i
                                       left join default.usage_data u
                                       on i.nexus_id = u.nexus_id
                                       and i.impression_date = u.request_date
                                       where i.impression_date between '%s' and '%s'
                                       and i.line_item in
                                       (
                                       %s
                                       )

                                       union all

                                       select  distinct i.nexus_id, u.ip_address, i.creative_id, i.line_item,
                                       i.dfp_advertiser_id, i.ad_unit, i.timestamp, 1 as clicks, i.click_date as event_date
                                       from default.dfp_clicks i
                                       left join default.usage_data u
                                       on i.nexus_id = u.nexus_id
                                       and i.click_date = u.request_date
                                       where i.click_date between '%s' and '%s'
                                       and i.line_item in
                                       (
                                       %s
                                       )""" % (startPeriod, endPeriod, dfp_line_items, startPeriod, endPeriod, dfp_line_items)
        onNetworkBanners = pd.read_sql_query(onNetworkBannersQuery, conn) # save results to pandas DF
        onNetworkBanners.to_csv(account_id + '.csv', index=False, header=False)

    def loadBannersCsv(self, pgConn, dbSchema, account_id):
        pgCursor = pgConn.cursor()
        try:
            f = open(account_id + ".csv")
            print ("file opened: " + str(f))
            pgCursor.copy_expert("copy " + dbSchema + ".on_network_" + account_id + " FROM STDIN WITH DELIMITER ',' CSV", f)
            pgConn.commit()
            print("data loaded")
            f.close()
            os.remove(account_id + ".csv")
            print("file removed")
        except Exception as e:
            print("File not uploaded: " + str(e))
            f.close()
            pgConn.rollback()

    def loadBannersOrCreate(self, pgConn, dbSchema, account_id, action):
        pgCursor = pgConn.cursor()
        if action == 'create':
            print 'creating table'
            pgCursor.execute("select distinct account_name from ba.salesforce_ios where account_id = '%s'" % (account_id))
            account_name = pgCursor.fetchall()
            account_name = account_name[0]
            account_name = ''.join(map(str, account_name))
            account_name =  account_name.replace("'","")
            source = "https://gitlab.spice.spiceworks.com/business_analytics/infrastructure/tree/master/warehouse/bi_db/ba_click_impressions.bzwx_impr"
            pgQueries = {
            "001 DROP ON NETWORK Banners":"drop table if exists %s.on_network_%s" % (dbSchema, account_id),
            "002 CREATE ON NETWORK Banners":"""create table %s.on_network_%s
            (
            nexus_id varchar(255),
            ip_address varchar(255),
            creative_id bigint,
            dfp_line_item bigint,
            dfp_advertiser_id bigint,
            ad_unit bigint,
            event_timestamp bigint,
            clicks integer,
            event_date date
            )""" % (dbSchema, account_id),
            "003 OWNERSHIP":"""ALTER TABLE %s.on_network_%s OWNER TO usergroup_ba""" % (dbSchema, account_id),
            "004 USERGROUP_BA":"""GRANT ALL ON TABLE %s.on_network_%s TO usergroup_ba""" % (dbSchema, account_id),
            "005 USERGROUP_MKT_RESEARCH":"""GRANT ALL ON TABLE %s.on_network_%s TO usergroup_market_research""" % (dbSchema, account_id),
            "006 TABLE COMMENT":"""COMMENT ON TABLE %s.on_network_%s
            IS 'Author: Oscar Valles.  %s On Network Clicks & Impressions from ad-cluster.  Source: %s'""" % (dbSchema, account_id, account_name, source),
            "007":"COMMENT ON COLUMN %s.on_network_%s.clicks IS 'clicks double counted when a nexus_id had more than 1 IP associated that day'" % (dbSchema, account_id),
            "008":"COMMENT ON COLUMN %s.on_network_%s.event_date IS 'event date is the date of the click or impression'" % (dbSchema, account_id),
            "009 INDEX":"""create index idx_%s_on_network_%s_event_date on %s.on_network_%s (event_date)""" % (dbSchema, account_id, dbSchema, account_id)
            }
            # execute above queries to create tables
            for d,q in sorted(pgQueries.iteritems()):
                try:
                    pgCursor.execute(q)
                    pgConn.commit()
                except Exception as e:
                    print("Postgres error: " + str(e))
                    pgConn.rollback()
            # load Banners
            self.loadBannersCsv(pgConn, dbSchema, account_id)
        else:
            print 'loading file'
            self.loadBannersCsv(pgConn, dbSchema, account_id)

        message = 'on_network_' + account_id + ' loaded'
        return message

    def csvBannersToTable(self, pgConn, dbSchema, account_id):
        dbCursor = pgConn.cursor()
        dbCursor.execute("""select count(*)
            from information_schema.columns
            where table_schema = '%s'
            and table_name = 'on_network_%s'""" % (dbSchema, account_id.lower()))
        result = dbCursor.fetchone()

        if result[0] > 0:
            msg = 'preparing to load file'
            action = 'load'
            self.loadBannersOrCreate(pgConn, dbSchema, account_id, action)
            dbCursor.close()
            return msg
        else:
            msg = 'creating table %s.on_network_%s for file load' % (dbSchema, account_id.lower())
            action = 'create'
            self.loadBannersOrCreate(pgConn, dbSchema, account_id, action)
            dbCursor.close()
            return msg
