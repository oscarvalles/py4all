# import needed libraries
import time, datetime as dt, psycopg2 as pg, os, csv, codecs, sys, pandas as pd
from datetime import timedelta
from pyhive import presto

from pyhive import presto
# custom library

from inc.on_network_banners import onNetwork
reload(sys)
sys.setdefaultencoding('utf-8')

# Establish Connections and Cursors
clusterConn = presto.connect('ad-cluster1',port=8083)
clusterCursor = clusterConn.cursor()

pgConnBidb = pg.connect(host='warehouse', database='bi_db', user='oscarv')
pgCursorBidb = pgConnBidb.cursor()

pgConnAh = pg.connect(host='warehouse', database='analytics_historical', user='oscarv')
pgCursorAh = pgConnAh.cursor()

# time components for report
day = dt.datetime.now().date()
# default impression dates
if not os.getenv('START_DATE'):
    start_date = dt.datetime.now().date() - timedelta(days=1, weeks=0) # yesterday
    start_date = str(start_date)
else:
    start_date = os.getenv('START_DATE')

if not os.getenv('END_DATE'):
    end_date =   dt.datetime.now().date() - timedelta(days=1, weeks=0) # yesterday
    end_date = str(end_date)
else:
    end_date = os.getenv('END_DATE')

# default schema
if not os.getenv('SCHEMA'):
    schema = 'ba_clicks_impressions'
else:
    schema = os.getenv('SCHEMA')

# accounts to bring in
# default schema
if not os.getenv('SFDC_ACCOUNT_ID'):
    # Below: Snow Software, Jumpcloud, Automox, CDW, HPI
    # sfdcAccountIds = ['001C0000017uXlwIAE','001C0000018LiFnIAK','001C000001QDnOvIAL','0018000000NqzXeAAJ','001C000001R647nIAB']
    sfdcAccountIdsQuery = "select * from ba_clicks_impressions.impression_click_customers"
    sfdcAccountIds = pd.read_sql_query(sfdcAccountIdsQuery, pgConnBidb)
    sfdcAccountIds = sfdcAccountIds['account_id'].tolist()
else:
    sfdcAccountIds = os.getenv('SFDC_ACCOUNT_ID')
    sfdcAccountIds = sfdcAccountIds.split()

print start_date, end_date, schema

# initialize class
onNetwork = onNetwork()

for account_id in sfdcAccountIds:
    dfpLineItems = onNetwork.dfp_line_items(account_id, pgConnBidb)
    print dfpLineItems
    onNetwork.onNetworkBannersToCsv(account_id, start_date, end_date, dfpLineItems, clusterConn)
    print 'csv exported'
    onNetwork.csvBannersToTable(pgConnBidb, schema, account_id)
