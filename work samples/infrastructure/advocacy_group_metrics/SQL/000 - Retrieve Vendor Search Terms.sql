﻿-- Retrieving search terms from vendor metrics
drop table if exists oscarv.vendor_search_terms;
create table oscarv.vendor_search_terms as
select vp.name
,substring(vm.search_expression_used from 'to_tsquery(.+) query WHERE') as tsquery_used
,substring(vm.search_expression_used from 'topics.subject(.+) OR ') as ilikes_uses
--,vm.search_expression_used
from public.vendor_metrics vm
inner join public.vendor_pages vp
 on vm.vendor_page_id = vp.id
where vm.year_month = '2017_5'
group by 1,2,3
order by 1
;

