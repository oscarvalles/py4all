-- Advocacy Participants
-- Rewrite to include community group name, group_id and search terms. Propogate name and id across all tables
drop table if exists ba.advocacy_participants;
create table ba.advocacy_participants as
(
  select distinct gm.user_id
  ,egs.user_name
  ,u.url_name
  ,egs.firstname || ' ' || egs.lastname as fullname
  ,egs.country_id
  ,egs.country
  ,g.name advocacy_group
  ,gm.group_id
  ,gm.created_at as join_date
  ,''::text as search_terms
  from public.group_memberships gm
  inner join email_grades egs
   on gm.user_id = egs.user_id
  inner join public.groups g
   on gm.group_id = g.id
  inner join public.users u
   on u.id = gm.user_id
  where (g.name ilike '%hp%heroes%'
  or g.name ilike '%hpi%print%'
  or g.name ilike '%knowbe4%'
  or g.name ilike '%netgear gear heads%'
  or g.name ilike '%cisco advocates%') -- group_names
  and group_role = 'Member'  -- excludes Admins of the group
  and user_role in ('User', 'Superuser') -- limits to user and superuser to control for employees/CBR's who are also members of the group
  order by 4
);

-- Update Search Terms
-- Will need to add distinguishing terms to the various HP Groups
-- update ba.advocacy_participants -- american hp groups
-- set search_terms = 'elitebook | laserjet | officejet | deskjet | compaq | presario | brio | deskpro'
-- where group_id in (754,1191)
-- ;
update ba.advocacy_participants -- hp emea heroes X3, hp heroes (U.S.)
set search_terms = 'x2 | x3 | x360 | elitebook | eliteone | (hp & elite) | (hp & laptop) | (hp & notebook) | (hp & pc & business) | (hp & desktop) | (hp & sure & start) | (hp & bios) | elitedesk | (hp & workstation) | a3 | pagewide | officejet | inkjet | laserjet | (hp & printer) | surestart | (managed & print & services) | (hp & mps) | (hp & jetadvantage) | jetadvantage | (hp & security & manager)'
where group_id in (1189,754)
;
update ba.advocacy_participants -- hp emea print and hp heroes print
set search_terms = 'x2 | x3 | x360 | elitebook | eliteone | (hp & elite) | (hp & laptop) | (hp & notebook) | (hp & pc & business) | (hp & desktop) | (hp & sure & start) | (hp & bios) | elitedesk | (hp & workstation) | a3 | pagewide | officejet | inkjet | laserjet | (hp & printer) | surestart | (managed & print & services) | (hp & mps) | (hp & jetadvantage) | jetadvantage | (hp & security & manager)'
where group_id in (1192,1191)
;
update ba.advocacy_participants
set search_terms = 'knowbe4 | instantrevert | (kevin & mitnick & security & awareness)'
where group_id in (1190)
;
update ba.advocacy_participants
set search_terms = 'netgear | readynas | rangemax'
where group_id in (1193)
;
update ba.advocacy_participants
set search_terms = ' meraki | (cisco & warranty) | (mobility & express) | aironet | (cisco & ap) | (cisco & router) | (cisco & wireless) | (cisco & switch) | (cisco & access & point) | cmx | (cisco & catalyst)'
where group_id in (1250)
;

-- Base table for followers/friends
drop table if exists ba.advocacy_friends;
create table ba.advocacy_friends as
	(
		select f.user_id, f.friends_id, f.created_at, ap.country, ap.advocacy_group, ap.group_id
		from public.friends f
		inner join ba.advocacy_participants ap
		  on f.friends_id = ap.user_id
	)
;

-- Get all topics and posts of a brand in the past 6 months
-- Will Need to Loop through this one for every Advocacy Group
drop table if exists ba.advocacy_group_posts;
create table ba.advocacy_group_posts as
(
select poi.advocacy_group, -- pass in value based on Advocacy group
poi.group_id,
t.forum_id,
p.topic_id,
p.id as post_id,
p.created_at,
p.user_id,
p.author_host::inet - '0.0.0.0'::inet as ip_bigint,
case when ap.user_id is not null and ap.group_id = poi.group_id then true else false end as advocate_post,
case when ta.answer_type = 'helpful' then 1 else 0 end as post_helpful,
case when ta.answer_type = 'best_answer' then 1 else 0 end as post_best_answer,
p.post_votes_count as post_spice_ups,
count(distinct case when ce.subscribed then ce.child_id::varchar(20) || ce.user_id::varchar(20) else null end) as post_subscriptions,
count(distinct case when ce.replied_to then ce.child_id::varchar(20) || ce.user_id::varchar(20) else null end) as post_replies,
''::varchar(10) as country_code,
''::varchar(255) as country
from public.posts p
inner join public.topics t
 on p.topic_id = t.id
inner join ( -- provides a combination of posts across all advocate groups
          select p.id, st.group_id, st.advocacy_group
              from public.posts p
              inner join public.topics t
               on p.topic_id = t.id
              inner join ba.community_group_summary cgs
               on cgs.forum_id = t.forum_id
              inner join (select distinct ap.search_terms, ap.group_id, ap.advocacy_group
                          from ba.advocacy_participants ap
                          --where ap.group_id = 1190
                         ) st
                on 1=1
               where (p.search_index @@ to_tsquery ('english', st.search_terms)
               or t.search_index @@ to_tsquery ('english', st.search_terms))
               and p.deleted = 0
               and t.deleted = 0
               and p.created_at::date between date_trunc('month',current_date) - interval '7 month' and current_date
               and cgs.category_tag != 'partners'  -- excludes posts made in advocate forums
               group by 1,2,3
          ) poi
 on poi.id = p.id
left join ba.advocacy_participants ap
 on p.user_id = ap.user_id
 -- and ap.group_id = 1190
left join public.topic_answers ta
 on p.id = ta.post_id
left join public.creeper_events ce
 on p.id = ce.child_id
where p.deleted = 0
and t.deleted = 0
and p.created_at::date between date_trunc('month',current_date) - interval '7 month' and current_date
group by 1,2,3,4,5,6,7,8,9,10,11,12
);

create index idxPstIpInt on ba.advocacy_group_posts (ip_bigint);
create index idxMain on ba.advocacy_group_posts (advocacy_group, user_id, topic_id);
create index idxPostDate on ba.advocacy_group_posts (created_at);

update ba.advocacy_group_posts
  set country_code = r.country_code,
  country = r.country
  from ba.ip_country_range r
  where ip_bigint between r.starting_range and r.ending_range
;
-- Topic Level Metrics
/*
drop table if exists ba.advocacy_group_topic_metrics;
create table ba.advocacy_group_topic_metrics as
(
select p.topic_id,
count(distinct case when ts.created_at >= p.created_at then ts.user_id else null end) as topic_subscriptions,
count(distinct case when tv.created_at >= p.created_at then tv.user_id else null end) as topic_spice_ups
from ba.advocacy_group_posts p
left join public.topic_subscriptions ts
 on ts.topic_id = p.topic_id
left join public.topic_votes tv
 on tv.topic_id = p.topic_id
group by 1
);*/

-- Create table of summary views (20 minutes)
drop table if exists ba.advocacy_topic_views;
create table ba.advocacy_topic_views as
(
select p.advocacy_group,
p.group_id,
coalesce(v.user_id, -1) as user_id,
v.ip_address::inet - '0.0.0.0'::inet as ip_bigint,
p.topic_id,
date_trunc('hour', v.created_at) pageview_month,
''::varchar(10) as country_code,
''::varchar(255) as country,
count(distinct v.id) as pageviews,
count(distinct v.ip_address) as unique_pageviews,
cast(max(case when p.created_at <= v.created_at then cast(p.advocate_post as int) else 0 end) as boolean) as contains_advocate_post
from ba.advocacy_group_posts p
inner join public.recent_csviews v
 on v.field = p.topic_id::text
where v.controller = 'topic'
group by 1,2,3,4,5,6
)
;

-- Execution Time: (1 minute)
create index idxViewIpInt on ba.advocacy_topic_views (ip_bigint);
create index idxViewsMain on ba.advocacy_topic_views (advocacy_group, user_id, topic_id);
create index idxViewsDate on ba.advocacy_topic_views (pageview_month);

-- 4 Hour Execution Time
update ba.advocacy_topic_views
  set country_code = r.country_code,
  country = r.country
  from ba.ip_country_range r
  where ip_bigint between r.starting_range and r.ending_range
;

-- Summary table used for charts and exports (2 minutes)
drop table if exists ba.advocacy_metrics_summary;
create table ba.advocacy_metrics_summary as
(
  -- Metrics Summary
  -- Advocate Mentions
  select p.advocacy_group
  ,p.group_id
  ,date_trunc('month',p.created_at) as time_period
  ,'Advocate Mentions'::varchar(100) as label
  ,p.country_code
  ,p.country
  ,count(distinct p.post_id) as measure
  from ba.advocacy_group_posts p
  where p.advocate_post
  group by 1,2,3,4,5,6
  union
  -- Advocate Views
  select v.advocacy_group
  ,v.group_id
  ,date_trunc('month',v.pageview_month) as time_period
  ,'Views of Advocate Mentions'::varchar(100) as label
  ,v.country_code
  ,v.country
  ,sum(v.pageviews) as measure
  from ba.advocacy_topic_views v
  where contains_advocate_post
  group by 1,2,3,4,5,6
  union
  -- Influenced Mentions
  select p.advocacy_group
  ,p.group_id
  ,date_trunc('month',p.created_at) as time_period
  ,'Influenced Posts'::varchar(100) as label
  ,p.country_code
  ,p.country
  ,count(distinct p.post_id) as measure
  from ba.advocacy_group_posts p
  where exists
   ( -- filter for advocate posts viewed (excluding views by advocate)
    select null
    from ba.advocacy_topic_views v
    where p.user_id = v.user_id
    and v.pageview_month >= p.created_at
    and p.user_id not in
       (
        select a.user_id
        from ba.advocacy_participants a
        group by 1
       )
    and v.contains_advocate_post
   )
   group by 1,2,3,4,5,6
   union
   -- Influenced Views
  select v.advocacy_group
  ,v.group_id
  ,date_trunc('month',v.pageview_month) as time_period
  ,'Views of Influenced Posts'::varchar(100) as label
  ,v.country_code
  ,v.country
  ,sum(v.pageviews) as measure
  from ba.advocacy_topic_views v
  where exists
  (
   select null
   from ba.advocacy_group_posts p
   where v.topic_id = p.topic_id
   and v.pageview_month >= p.created_at
   and exists
    ( -- filter for advocate posts viewed (excluding views by advocate)
     select null
     from ba.advocacy_topic_views v
     where p.user_id = v.user_id
     and v.pageview_month >= p.created_at
     and p.user_id not in
        ( -- excludes views by advocates
         select a.user_id
         from ba.advocacy_participants a
         group by 1
        )
     and v.contains_advocate_post
    )
   group by 1
  )
  group by 1,2,3,4,5,6
  union
  -- Followers
  -- cumulative up to six months back
  select f.advocacy_group
  ,f.group_id
  ,date_trunc('month', current_date - interval '6 month' ) as time_period
  ,'Followers'::varchar(100) as label
  ,''::varchar(10) country_code
  ,f.country
  ,count(distinct f.user_id) as measure
  from ba.advocacy_friends f
  -- where advocacy_group = 'KnowBe4 Corps'
  where f.created_at::date <= date_trunc('month',current_date) - interval '6 month'
  group by 1,2,3,4,5,6
  union
  -- Followers: last 5 months
  select f.advocacy_group
  ,f.group_id
  ,date_trunc('month',f.created_at) as time_period
  ,'Followers'::varchar(100) as label
  ,''::varchar(10) country_code
  ,''::varchar(100) country
  ,count(distinct f.user_id) as measure
  from ba.advocacy_friends f
  -- where advocacy_group = 'KnowBe4 Corps'
  where f.created_at::date between date_trunc('month',current_date) - interval '5 month' and current_date
  group by 1,2,3,4,5,6
  union
  -- Engagements
  select p.advocacy_group
  ,p.group_id
  ,date_trunc('month',p.created_at) as time_period
  ,'Engagement'::varchar(100) as label
  ,p.country_code
  ,p.country
  ,sum(p.post_helpful +
       p.post_best_answer +
       p.post_spice_ups +
       p.post_subscriptions +
       p.post_replies) as measure
  from ba.advocacy_group_posts p
  where p.advocate_post
  group by 1,2,3,4,5,6
  union
  -- Green Gal/Guy Posts
  select p.advocacy_group
  ,p.group_id
  ,date_trunc('month',p.created_at) as time_period
  ,'Green Guy Mentions'::varchar(100) as label
  ,p.country_code
  ,p.country
  ,count(distinct p.post_id) as measure
  from ba.advocacy_group_posts p
  where user_id = 4530445
  group by 1,2,3,4,5,6
  union
  -- Green Guy/Gal Pageviews
  select v.advocacy_group
  ,v.group_id
  ,date_trunc('month',v.pageview_month) as time_period
  ,'Green Guy Pageviews'::varchar(100) as label
  ,v.country_code
  ,v.country
  ,sum(v.pageviews) as pageviews
  from ba.advocacy_topic_views v
  where exists
   (
    select null
    from ba.advocacy_group_posts p
    where p.topic_id = v.topic_id
    and p.created_at <= v.pageview_month
    and p.user_id = 4530445
   )
  group by 1,2,3,4,5,6
  union
  -- GG Engagements
  select p.advocacy_group
  ,p.group_id
  ,date_trunc('month',p.created_at) as time_period
  ,'Green Guy Engagement'::varchar(100) as label
  ,p.country_code
  ,p.country
  ,sum(p.post_helpful +
       p.post_best_answer +
       p.post_spice_ups +
       p.post_subscriptions +
       p.post_replies) as measure
  from ba.advocacy_group_posts p
  inner join ba.advocacy_participants ap
   on p.group_id = ap.group_id
   and p.user_id = ap.user_id
   and p.advocacy_group = ap.advocacy_group
  where p.user_id in (select u.id --, vp.name, vpm.role, u.role, u.name, u.url_name
                       from public.vendor_page_memberships vpm
                       inner join public.vendor_pages vp
                        on vpm.vendor_page_id = vp.id
                       inner join public.users u
                        on vpm.user_id = u.id
                       where lower(vp.name) in ('hewlettpackardenterprise','hp','knowbe4','netgear')
                       and lower(u.role) in (/*'marketer',*/ 'partner')
                       and vpm.role = 'Admin'
                       and u.name not ilike '% for %'
                    )--= 4530445
  group by 1,2,3,4,5,6
);

alter table ba.advocacy_metrics_summary
  owner to usergroup_ba;
grant all on table ba.advocacy_metrics_summary to usergroup_ba;
grant select on table ba.advocacy_metrics_summary to report;
grant select on table ba.advocacy_metrics_summary to portal;
grant select on table ba.advocacy_metrics_summary to skypal_ro;
grant select on table ba.advocacy_metrics_summary to skypal_dev;
grant select on table ba.advocacy_metrics_summary to skypal_mkt;
grant select on table ba.advocacy_metrics_summary to group_an_hist_ba_schema_ro;
  COMMENT ON TABLE ba.advocacy_metrics_summary
    IS 'Author: Oscar Valles.  Used in: http://ba-ws/advocacy/ Source Code: https://gitlab.spice.spiceworks.com/oscarv/projects';

alter table ba.advocacy_friends
  owner to usergroup_ba;
grant all on table ba.advocacy_friends to usergroup_ba;
grant select on table ba.advocacy_friends to report;
grant select on table ba.advocacy_friends to portal;
grant select on table ba.advocacy_friends to skypal_ro;
grant select on table ba.advocacy_friends to skypal_dev;
grant select on table ba.advocacy_friends to skypal_mkt;
grant select on table ba.advocacy_friends to group_an_hist_ba_schema_ro;
  COMMENT ON TABLE ba.advocacy_friends
    IS 'Author: Oscar Valles.  Used in: http://ba-ws/advocacy/ Source Code: https://gitlab.spice.spiceworks.com/oscarv/projects';

alter table ba.advocacy_group_posts
  owner to usergroup_ba;
grant all on table ba.advocacy_group_posts to usergroup_ba;
grant select on table ba.advocacy_group_posts to report;
grant select on table ba.advocacy_group_posts to portal;
grant select on table ba.advocacy_group_posts to skypal_ro;
grant select on table ba.advocacy_group_posts to skypal_dev;
grant select on table ba.advocacy_group_posts to skypal_mkt;
grant select on table ba.advocacy_group_posts to group_an_hist_ba_schema_ro;
  COMMENT ON TABLE ba.advocacy_group_posts
    IS 'Author: Oscar Valles.  Used in: http://ba-ws/advocacy/ Source Code: https://gitlab.spice.spiceworks.com/oscarv/projects';

alter table ba.advocacy_participants
  owner to usergroup_ba;
grant all on table ba.advocacy_participants to usergroup_ba;
grant select on table ba.advocacy_participants to report;
grant select on table ba.advocacy_participants to portal;
grant select on table ba.advocacy_participants to skypal_ro;
grant select on table ba.advocacy_participants to skypal_dev;
grant select on table ba.advocacy_participants to skypal_mkt;
grant select on table ba.advocacy_participants to group_an_hist_ba_schema_ro;
  COMMENT ON TABLE ba.advocacy_participants
    IS 'Author: Oscar Valles.  Used in: http://ba-ws/advocacy/ Source Code: https://gitlab.spice.spiceworks.com/oscarv/projects';

alter table ba.advocacy_topic_views
  owner to usergroup_ba;
grant all on table ba.advocacy_topic_views to usergroup_ba;
grant select on table ba.advocacy_topic_views to report;
grant select on table ba.advocacy_topic_views to portal;
grant select on table ba.advocacy_topic_views to skypal_ro;
grant select on table ba.advocacy_topic_views to skypal_dev;
grant select on table ba.advocacy_topic_views to skypal_mkt;
grant select on table ba.advocacy_topic_views to group_an_hist_ba_schema_ro;
  COMMENT ON TABLE ba.advocacy_topic_views
    IS 'Author: Oscar Valles.  Used in: http://ba-ws/advocacy/ Source Code: https://gitlab.spice.spiceworks.com/oscarv/projects';
