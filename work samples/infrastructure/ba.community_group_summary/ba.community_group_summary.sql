
drop table if exists ba.community_group_summary;
create table ba.community_group_summary as
(
select
gc.name as category_name
,gc.url_name as category_tag
,'https://community.spiceworks.com/' || gc.url_name as category_url
,f.id as forum_id
,f.name as forum_name
,g.id as group_id
,g.name as group_name
,g.url_name as group_tag
,'https://community.spiceworks.com/'|| gc.url_name || '/' || g.url_name as group_url
,g.auto_tags as group_auto_tags
,max(g.member_count) as total_group_followers
,current_date as refresh_date
from production.groups g
inner join production.group_categories gc
	on g.group_category_id = gc.id
inner join production.forums f
	on g.forum_id = f.id
group by 1,2,3,4,5,6,7,8,9,10
)
;

CREATE index idxGroupId on ba.community_group_summary (group_id);
CREATE index idxForumId on ba.community_group_summary (forum_id);

GRANT ALL ON TABLE ba.community_group_summary TO usergroup_ba;
GRANT SELECT ON TABLE ba.community_group_summary TO report;
GRANT SELECT ON TABLE ba.community_group_summary TO portal;
GRANT SELECT ON TABLE ba.community_group_summary TO skypal_ro;
GRANT SELECT ON TABLE ba.community_group_summary TO skypal_dev;
GRANT SELECT ON TABLE ba.community_group_summary TO skypal_mkt;
ALTER table ba.community_group_summary
  OWNER TO usergroup_ba;
COMMENT ON TABLE ba.community_group_summary
	IS 'Author: Oscar Valles.  Purpose: Lists out categories and forum properties for topics via forum_id';
