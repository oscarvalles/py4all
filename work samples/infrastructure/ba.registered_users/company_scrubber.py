"""
# Name: Company Name Scrubber
# Purpose: Scrub User and Duns company names found in ba.registered_users.
#          To run after the ba.registered_users table is built
# Author: Oscar Valles
# Last Update Date: 11/22/2017
"""
import psycopg2 as pg, pandas as pd, numpy as np

# connections and cursors
biDbConn = pg.connect(host='warehouse', database='bi_db', user='oscarv')
biDbConn.set_isolation_level(pg.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
biDbCursor = biDbConn.cursor()
ahConn = pg.connect(host='warehouse', database='analytics_historical', user='oscarv')
ahCursor = ahConn.cursor()

# retrieve all registrant records with a company name to scrub
companyCleanQuery = """select user_id, coalesce(lower(company), '--') as company
from ba.registered_users
where company != ''
and company != '--'
and company is not null
"""
biDbCursor.execute(companyCleanQuery)
results = biDbCursor.fetchall()

# substrings to remove from company names in this order.  Order very important
orgs = {'001':'.', '002':',', '003':"'", '004':'(', '005': ')', '006':' &', '007':' bv', '008':' llc',
        '009':' inc', '010':' corp', '011':' llp', '012':' limited', '013':' ltd', '014':' plc',
        '015':' corporation', '016':' sa', '017':' ltda', '018':' ltda me', '019':' ag', '020':' gmbh',
        '021':' sa de cv', '020':' co', '021':' lp', '022':' pc', '023':' pty', '024':' sl'}
scrubbed_org = ''
user_id = 0

# update scrubbed names into table
for r in results:
    scrubbed_org = r[1]
    user_id = r[0]
    for i,o in sorted(orgs.iteritems()):
        scrubbed_org = scrubbed_org.replace(o,'').strip()
    updateQuery = """update ba.registered_users set scrubbed_company = '%s' where user_id = %s""" % (scrubbed_org,user_id)
    biDbCursor.execute(updateQuery)
    # set next user_id and company to be scrubbed
    scrubbed_org = r[1]
    user_id = r[0]
    print scrubbed_org + 'processed'
