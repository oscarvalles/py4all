/*******
* Data Set Name: ba.registered_users
* Purpose: create a base table of community users with components such as email eligibility, duns, domain and scrubbed company name for matching along with 3rd party data
* Author: Oscar Valles
* Last Update Date: 11/24/2017
* TODOs: -- update company size ranges (make consistent)
*        -- update revenue ranges (make consistent)
*        -- update industry values where sic code is present
*        -- update email preferences from pg source not ba.users_extended
*        -- use ml for deriving predicted_company_title (group by job role for valid entries, train & test on social data)
********/

-- tables being created for performance
drop table if exists ba.user_to_best_client;
create table ba.user_to_best_client as
  select * from production.user_to_best_client
;
create index idx_user_id_ba_user_to_best_client on ba.user_to_best_client (user_id);

drop table if exists ba.dnb_data;
create table ba.dnb_data as
select dnb.*
from internal_use_only.dnb_data dnb
inner join (
select duns, max(updated_at) as updated_at
from internal_use_only.dnb_data
group by duns
) m
 on dnb.duns = m.duns
 and dnb.updated_at = m.updated_at
;
create index idx_duns_ba_dnb_data on ba.dnb_data (duns);

-- base table
drop table if exists ba.registered_users;
create table ba.registered_users as
select distinct coalesce(ubc.client_id, -1) as client_id, cu.user_id, cu.u_name as username,
cu.up_profile_name as profile_name, up_company_title as company_title,
''::varchar as job_role,
''::varchar as predicted_job_role,
cu.up_company_name as company, ''::varchar as scrubbed_company,
i.name as industry, cs.size as company_size, r.revenue,
cu.up_city as city, cu.up_state as state, coalesce(c.maxmind_name,cu.up_country) as country,
d.domain, ''::varchar as duns, ubc.version, ubc.devices,
false::boolean as blacklist, false::boolean as newsletter, false::boolean as third_party, false::boolean as voit, false::boolean as active_last_90_days,
current_date as refresh_date
from production.community_users cu
inner join production.user_to_domain d
 on cu.user_id = d.user_id
left join production.users_to_company_attributes ca
 on cu.user_id = ca.user_id
left join production.company_names cn
 on ca.company_name_id = cn.id
left join production.countries c
 on c.sp_country_id = cu.u_country_id
left join production.company_revenues r
 on ca.revenue_id = r.id
left join production.industries i
 on i.id = ca.industry_id
left join production.company_sizes cs
 on cs.id = ca.size_id
left join oscarv.user_to_best_client ubc
 on ubc.user_id = cu.user_id
;

create index idx_user_id_ba_registered_users on ba.registered_users (user_id);
create index idx_domain_ba_registered_users on ba.registered_users (domain);

-- remove generic and bogus domains
update ba.registered_users
set domain = '--'
from ba.domain_exclusion d
where registered_users.domain = d.domain
;

-- insert missing duns numbers based on domains
update ba.registered_users
set duns = d.duns
from production.duns_to_domains d
where registered_users.domain = d.domain
and (registered_users.duns is null
  or registered_users.duns = '')
and d.domain not in (select * from ba.ba_domain_exclusion)
;

-- update available info based on new domain->duns data available
update ba.registered_users
set company = d.org_name,
industry = d.org_industry_code,
company_size = d.org_num_employees::varchar || '-' || d.org_num_employees::varchar,
revenue = d.org_revenue::varchar || '-' || d.org_revenue::varchar,
city = d.org_locality,
state = d.org_region,
country = d.org_country
from ba.dnb_data d
where registered_users.duns = d.duns
and registered_users.company is null
and registered_users.duns is not null
;

-- update the flag that the user is active in the last 90 days
update ba.registered_users
  set active_last_90_days = true
from public.user_usage_summary_data d
where registered_users.user_id = d.user_id
and d.pv_month_date between current_date - interval '3 month' and current_date
;

-- update with email preference data
update ba.registered_users
  set newsletter = case when d.newsletter = 1 then true else false end,
  third_party = case when d.third_party = 1 then true else false end,
  voit = case when d.voit = 1 then true else false end,
  blacklist = case when d.email_eligible = 0 then false else true end
from ba.users_extended d
where registered_users.user_id = d.user_id
and d.active_within_last_year
and d.email_eligible = 1
;

-- drop intermediary performance tables
drop table if exists ba.dnb_data;
drop table if exists ba.user_to_best_client;

-- permissions setting
grant all on ba.registered_users to usergroup_ba;

alter table ba.registered_users
  owner to usergroup_ba;
grant all on table ba.registered_users to usergroup_ba;
grant select on table ba.registered_users to report;
grant select on table ba.registered_users to portal;
grant select on table ba.registered_users to skypal_ro;
grant select on table ba.registered_users to skypal_dev;
grant select on table ba.registered_users to skypal_mkt;
grant select on table ba.registered_users to group_an_hist_ba_schema_ro;
grant select on table ba.registered_users to willt;
grant all on table ba.registered_users to etlr;
comment on table ba.registered_users
  is 'Author: Oscar Valles.  Purpose: Registered Users with attributes for bi_db. Source Code: https://gitlab.spice.spiceworks.com/business_analytics/ba_datasets/bi_db/ba.registered_users/ba.registered_users.sql';
