# ba.registered_users (bi_db)

Table of community users with email preference, 3rd party and scrubbed company names, duns and domain attributes

## Schedule:

Scheduled to build every Friday at 7:00 pm (cron)

### Purpose of this table

```
> Use for iPython Notebook Company Matcher (within this repo)
> Use to estimate available Email and Media Audiences
> Used for audience demographic summaries
> bi_db's replacement for analytics_historical's email_grades_summary
```

### Prerequisites

Software needed to run this process

```
PostgreSQL Shell
Python 2.7
 - Modules
    > psycopg2
    > pandas
    > numpy
```

### Caveats / TODOs

Things left to do for this dataset are:
```
  -- update company size ranges (make consistent)
  -- update revenue ranges (make consistent)
  -- update industry values where sic code is present
  -- update email preferences from pg source not ba.users_extended
  -- use ml for deriving predicted_company_title (group by job role for valid entries, train & test on social data)
```
