for i in {11..15} # start at 2 and go back as many days as you would like to load of data
  # do param_date=$(date -d ${i}' days ago' +%Y-%m-%d); # works on Ubuntu
  do param_date=$(date -v -${i}d +%Y-%m-%d); # works on OS X
  START_DATE=${param_date} END_DATE=${param_date} SFDC_ACCOUNT_ID='0018000000NqzXeAAJ' python beeswax_impressions.py
  # echo ${param_date};
done
