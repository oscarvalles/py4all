import  pandas as pd, numpy as np, time, datetime as dt, psycopg2 as pg, os, csv, codecs, sys

class beeswax():
    def __init__(self):
        return

    def dfp_line_items(self, account_id, conn):
        dfpLineItemsQuery = """select distinct d.line_item_id
            from ba.salesforce_ios s
            inner join ba.dfp_line_items d
            on d.io = s.io
            where s.account_id in ('""" + account_id + """')
            and d.line_item_id is not null"""

        dfpLineItems = pd.read_sql_query(dfpLineItemsQuery, conn)

        # save line items into a string to input later
        dfpLineItemList = dfpLineItems['line_item_id'].tolist()
        # dfpLineItemList = ",".join(map(str, dfpLineItemList))
        dfpLineItemList = ','.join(["(cast('%s' as bigint))" % line_item for line_item in dfpLineItemList])
        return dfpLineItemList

    def beeswaxImpressionsToCsv(self, account_id, startPeriod, endPeriod, dfp_line_items, conn):
        besswaxImpressionsQuery = """select distinct i.auction_id, m.nexus_id, '' as user_id, b.ip_address, '' as duns, i.dfp_line_item, b.clicks, i.date as impression_date
            from default.beeswax_dfp_impressions i
            inner join default.beeswax_impressions_batch b
            on i.auction_id = b.auction_id
            inner join default.beeswax_id_mapping m
            on replace(b.user_id,'bito.','') = m.beeswax_id
            where b.date between '""" + startPeriod + """' and '""" + endPeriod + """'
            and m.match_date between '""" + startPeriod + """' and '""" + endPeriod + """'
            and m.match_date <= i.date
            and i.dfp_line_item in (""" + dfp_line_items + """)

            union all

            select distinct i.auction_id, m.nexus_id, '' as user_id, b.ip_address, '' as duns, i.dfp_line_item, b.clicks, i.date as impression_date
                from default.beeswax_dfp_clicks i
                inner join default.beeswax_impressions_batch b
                on i.auction_id = b.auction_id
                inner join default.beeswax_id_mapping m
                on replace(b.user_id,'bito.','') = m.beeswax_id
                where b.date between '""" + startPeriod + """' and '""" + endPeriod + """'
                and m.match_date between '""" + startPeriod + """' and '""" + endPeriod + """'
                and m.match_date <= i.date
                and i.dfp_line_item in (""" + dfp_line_items + """)"""
        beeswaxImpressions = pd.read_sql_query(besswaxImpressionsQuery, conn) # save results to pandas DF
        beeswaxImpressions.to_csv(account_id + '.csv', index=False, header=False)

    def loadImpressionsCsv(self, pgConn, dbSchema, account_id):
        pgCursor = pgConn.cursor()
        try:
            f = open(account_id + ".csv")
            print ("file opened: " + str(f))
            pgCursor.copy_expert("copy " + dbSchema + ".beeswax_" + account_id + " FROM STDIN WITH DELIMITER ',' CSV", f)
            pgConn.commit()
            print("data loaded")
            f.close()
            os.remove(account_id + ".csv")
            print("file removed")
        except Exception as e:
            print("File not uploaded: " + str(e))
            f.close()
            pgConn.rollback()

    def loadImpressionsOrCreate(self, pgConn, dbSchema, account_id, action):
        pgCursor = pgConn.cursor()
        if action == 'create':
            print 'creating table'
            pgCursor.execute("select distinct account_name from ba.salesforce_ios where account_id = '%s'" % (account_id))
            account_name = pgCursor.fetchall()
            account_name = account_name[0]
            account_name = ''.join(map(str, account_name))
            account_name =  account_name.replace("'","")
            source = "https://gitlab.spice.spiceworks.com/business_analytics/infrastructure/tree/master/warehouse/bi_db/ba_click_impressions.bzwx_impr"
            pgQueries = {
            "001 DROP BEESWAX_IMPRESSIONS":"drop table if exists %s.beeswax_%s" % (dbSchema, account_id),
            "002 CREATE BEESWAX_IMPRESSIONS":"""create table %s.beeswax_%s
            (
            auction_id varchar(255),
            nexus_id varchar(255),
            user_id integer,
            ip_address varchar(255),
            duns varchar(255),
            dfp_line_item bigint,
            clicks integer,
            impression_date date
            )""" % (dbSchema, account_id),
            "003 OWNERSHIP":"""ALTER TABLE %s.beeswax_%s OWNER TO usergroup_ba""" % (dbSchema, account_id),
            "004 USERGROUP_BA":"""GRANT ALL ON TABLE %s.beeswax_%s TO usergroup_ba""" % (dbSchema, account_id),
            "005 USERGROUP_MKT_RESEARCH":"""GRANT ALL ON TABLE %s.beeswax_%s TO usergroup_market_research""" % (dbSchema, account_id),
            "006 TABLE COMMENT":"""COMMENT ON TABLE %s.beeswax_%s
            IS 'Author: Oscar Valles.  %s Beeswax Clicks & Impressions from ad-cluster.  Source: %s'""" % (dbSchema, account_id, account_name, source),
            "007":"COMMENT ON COLUMN %s.beeswax_%s.user_id IS 'user_id placeholder for backfill within Postgres'",
            "008":"COMMENT ON COLUMN %s.beeswax_%s.duns IS 'duns placeholder for backfill within Postgres'",
            "009 INDEX":"""create index idx_%s_beeswax_%s_impression_date on %s.beeswax_%s (impression_date)""" % (dbSchema, account_id,dbSchema, account_id)
            }
            # execute above queries to create tables
            for d,q in sorted(pgQueries.iteritems()):
                try:
                    pgCursor.execute(q)
                    pgConn.commit()
                except Exception as e:
                    print("Postgres error: " + str(e))
                    pgConn.rollback()
            # load impressions
            self.loadImpressionsCsv(pgConn, dbSchema, account_id)
        else:
            print 'loading file'
            self.loadImpressionsCsv(pgConn, dbSchema, account_id)

        message = 'beeswax_' + account_id + ' loaded'
        return message

    def csvImpressionsToTable(self, pgConn, dbSchema, account_id):
        dbCursor = pgConn.cursor()
        dbCursor.execute("""select count(*)
            from information_schema.columns
            where table_schema = '%s'
            and table_name = 'beeswax_%s'""" % (dbSchema, account_id.lower()))
        result = dbCursor.fetchone()

        if result[0] > 0:
            msg = 'preparing to load file'
            action = 'load'
            self.loadImpressionsOrCreate(pgConn, dbSchema, account_id, action)
            dbCursor.close()
            return msg
        else:
            msg = 'creating table %s.beeswax_%s for file load' % (dbSchema, account_id.lower())
            action = 'create'
            self.loadImpressionsOrCreate(pgConn, dbSchema, account_id, action)
            dbCursor.close()
            return msg
